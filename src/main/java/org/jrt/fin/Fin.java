package org.jrt.fin;

import org.jrt.fin.phones.InheritablePhone;
import org.jrt.fin.phones.NonInheritablePhone;
import org.jrt.fin.phones.SubclassedPhone;

/**
 * class to experiment with the final keyword
 * @author jrtobac
 *
 */
public class Fin {
	public static void main(String[] args) {
		InheritablePhone ip = new SubclassedPhone();
		ip.call();
		ip.updatePhone();
		System.out.println("The color of this sublassed phone is: " + ip.getColor());
		System.out.println("The total numbers on the subclassed phone is: " + ip.getNumbersOnPhone());
		System.out.println();
		
		NonInheritablePhone np = new NonInheritablePhone();
		np.call();
		np.updatePhone();
		System.out.println("The color of this non-inheritable phone is: " + np.getColor());
		System.out.println("The total numbers on the non-inheritable phone is: " + np.getNumbersOnPhone());
	}
}
