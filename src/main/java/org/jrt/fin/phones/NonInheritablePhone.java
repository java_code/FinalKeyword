package org.jrt.fin.phones;

/**
 * class representing a phone that cannot be inherited 
 * 
 * @author jrtobac
 *
 */
public final class NonInheritablePhone {
	private final int numbersOnPhone=10;
	private  String color = "black";
	
	/**
	 * Alert the user that the phone is calling someone
	 */
	public final void call() {
		System.out.println("NonInheritablePhone is Calling...");
	}
	
	/**
	 * update the phone to a different color
	 */
	public void updatePhone() {
		color="orange";
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the numbersOnPhone
	 */
	public int getNumbersOnPhone() {
		return numbersOnPhone;
	}	
}
