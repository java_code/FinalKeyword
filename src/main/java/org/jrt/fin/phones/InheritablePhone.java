package org.jrt.fin.phones;

/**
 * class representing a phone that can be inherited
 * 
 * @author jrtobac
 *
 */
public class InheritablePhone {

	//private final int numbersOnPhone;//The blank final field numbersOnPhone may not have been initialized
	private final int numbersOnPhone=10;
	protected String color = "black";
	
	/**
	 * Alert the user that the phone is calling someone
	 */
	public final void call() {
		System.out.println("InheritablePhone is Calling...");
	}
	
	/**
	 * update the phone to a different color
	 */
	public void updatePhone() {
		//numbersOnPhone = 20;//The final field Phone.numbersOnPhone cannot be assigned
		color="green";
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the numbersOnPhone
	 */
	public int getNumbersOnPhone() {
		return numbersOnPhone;
	}	
}
