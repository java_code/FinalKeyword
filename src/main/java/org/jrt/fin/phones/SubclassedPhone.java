package org.jrt.fin.phones;

/**
 * class representing a phone that is a subclass of another phone class
 * @author jrtobac
 *
 */
//public class SubclassedPhone extends NonInheritablePhone {//The type SubclassedPhone cannot subclass the final class NonInheritablePhone
  public class SubclassedPhone extends InheritablePhone{
	  
	  /**
	   * update the phone to a different color
	   */
	  public void updatePhone() {
		  super.color="purple";
	  }
	  
	  /*@Override
	  public void call() {//Cannot override the final method from InheritablePhone
		  System.out.println("Ring Ring");
	  }*/  
	  
}
