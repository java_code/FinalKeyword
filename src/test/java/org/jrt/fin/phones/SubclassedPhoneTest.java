package org.jrt.fin.phones;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SubclassedPhoneTest {

	InheritablePhone sp = new SubclassedPhone();
	
	/**
	 * test that the phone updates its color to purple
	 */
	@Test
	void testUpdatePhone_updatesToPurple() {
		String expected = "purple";
		sp.updatePhone();
		assertEquals(expected, sp.getColor());
	}

}
