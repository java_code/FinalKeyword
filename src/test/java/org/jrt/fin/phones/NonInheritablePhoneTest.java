package org.jrt.fin.phones;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class NonInheritablePhoneTest {

	NonInheritablePhone nip = new NonInheritablePhone();
	
	/**
	 * test that the phone calls correctly
	 */
	@Test
	void testCall_printsThatItIsCalling() {
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		nip.call();
		assertEquals("NonInheritablePhone is Calling...", outContent.toString().trim());
	}
	
	/**
	 * test that the phone turns orange on update
	 */
	@Test
	void testUpdatePhone_turnsPhoneGreen() {
		String expected = "orange";
		nip.updatePhone();
		assertEquals(expected, nip.getColor());
	}

}
