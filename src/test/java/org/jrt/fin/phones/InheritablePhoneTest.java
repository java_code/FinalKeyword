package org.jrt.fin.phones;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class InheritablePhoneTest {

	InheritablePhone ip = new InheritablePhone();
		
	/**
	 * test that the phone calls correctly
	 */
	@Test
	void testCall_printsThatItIsCalling() {
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		ip.call();
		assertEquals("InheritablePhone is Calling...", outContent.toString().trim());
	}
	
	/**
	 * test that the phone turns green on update
	 */
	@Test
	void testUpdatePhone_turnsPhoneGreen() {
		String expected = "green";
		ip.updatePhone();
		assertEquals(expected, ip.getColor());
	}

}
